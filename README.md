# OpenML dataset: Housing-Prices-in-London

https://www.openml.org/d/43416

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Content
This dataset comprises of various house listings in London and neighbouring region. It also encompasses the parameters listed below, the definitions of which are quite self-explanatory.
    Property Name
    Price
    House Type - Contains one of the following types of houses (House, Flat/Apartment, New Development, Duplex, Penthouse, Studio, Bungalow, Mews)
    Area in sq ft
    No. of Bedrooms
    No. of Bathrooms
    No. of Receptions
    Location
    City/County - Includes London, Essex, Middlesex, Hertfordshire, Kent, and Surrey.
    Postal Code 
Inspiration
This dataset has various parameters for each house listing which can be used to conduct Exploratory Data Analysis. It can also be used to predict the house prices in various regions of London by means of Regression Analysis or other learning methods.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43416) of an [OpenML dataset](https://www.openml.org/d/43416). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43416/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43416/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43416/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

